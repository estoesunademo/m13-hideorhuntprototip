import { pool } from ".";
import { LogStatus, msg } from "./Alert";
import bcrypt from "bcrypt";

export async function loginUser(username: string, password: string): Promise<[string, LogStatus, boolean]> {
    try {
        const [results] = await pool.query(
            'SELECT username, password FROM Users WHERE username = ?',
            [username]
        ) as any[];
        if (results.length === 0) {
            return [msg.user_or_password_wrong, "warning", false];
        }
        const passwordsMatch: boolean = await bcrypt.compare(password, results[0].password);
        if (!passwordsMatch) {
            return [msg.user_or_password_wrong, "warning", false];
        }
    } catch (err) {
        console.log(err);
        return [msg.unexpected, "error", false];
    }
    return [msg.logged_success, "success", true];
}
