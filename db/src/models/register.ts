import { pool } from "..";
import bcrypt from "bcrypt";
import { LogStatus, msg } from "../Alert";

export async function registerUser(username: string, password: string): Promise<[string, LogStatus]> {
    try {
        const [existingUsers] = await pool.query(
            'SELECT (username) FROM Users WHERE username = ?',
            [username]
        ) as any[];
        if (existingUsers.length > 0) {
            return [msg.already_exists, "warning"];
        }
        const hashedPassword = await bcrypt.hash(password, 10);

        const [createdUser] = await pool.query(
            'INSERT INTO Users (username, password) VALUES (?, ?)',
            [username, hashedPassword]
        );
        console.log(createdUser);
    } catch (err) {
        console.log(err);
        return [msg.unexpected, "error"];
    }
    return [msg.user_created_success, "success"];
}
