import { pool } from "..";

type User = {
    username: string,
    score: number
}

type DBUser = {
    username: string,
    score: number,
    played: number,
}

export async function submitScores(password: string, data: User[]): Promise<boolean> {
    if (password !== process.env.GAME_PASSWORD) return false;
    try {
        const [users] = await pool.query('SELECT username, score, played FROM Users') as any[];
        users.forEach(async (dbUser: DBUser) => {
            const dataUser = data.find((du) => du.username === dbUser.username);
            if (dataUser) {
                dbUser.score += dataUser.score;
                dbUser.played++;
                await pool.query(
                    'UPDATE Users SET score = ?, played = ? WHERE username = ?',
                    [dbUser.score, dbUser.played, dbUser.username]
                )
            }
        })
    } catch (err) {
        console.log(err);
        return false;
    }
    return true;
}
