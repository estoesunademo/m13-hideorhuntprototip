import Footer from "./Footer";
import Navbar from "./Navbar";

type Props = {
    children: any;
}

const BaseHtml = ({ children }: Props) => {
    return (
        <html class="scroll-smooth">
            <head>
                <title>HideOrHunt</title>
                <meta charset="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="icon" href="/public/favicon.ico" />
                <link rel="stylesheet" href="/public/output.css" type="text/css" />
                <script src="https://unpkg.com/htmx.org@1.9.12" />
            </head>
            <body class="bg-base-300">
                <Navbar />
                <main class="flex flex-col items-center p-4" id="main">
                    {children}
                </main>
                <Footer />
            </body>
        </html>
    );
}

export default BaseHtml;
