import { pool } from "..";

async function Home() {

    const [users]: any[] = await pool.query("SELECT user_id, username, played, score FROM Users ORDER BY score DESC");

    return (
        <div class="text-xl">
            <h1>Home</h1>
            <div class="overflow-x-auto">
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Username</th>
                            <th>Games Played</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user: any) => (
                            <tr>
                                <th>{user.user_id}</th>
                                <td>{user.username}</td>
                                <td>{user.played}</td>
                                <td>{user.score}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default Home;
