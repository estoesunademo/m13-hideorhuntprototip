function Hosting() {
    return (
        <div>
            <h1 class="text-4xl text-center mb-8">
                Hosting
            </h1>
            <div role="tablist" class="tabs tabs-lifted">
                <input type="radio" name="tab-group" role="tab" class="tab" aria-label="Windows" checked />
                <div role="tabpanel" class="tab-content bg-base-100 border-base-300 rounded-box p-8">
                    <h2 class="text-2xl text-center">
                        🪟 Windows 10/11
                    </h2>
                    <div class="divider" />
                    <div class="flex flex-col gap-4">
                        <h3 class="text-xl text-center font-bold">
                            Prerequisites:
                        </h3>
                        <div>1. git (<a class="link link-info" href="https://git-scm.com/download/win">Download</a> &amp; Install)</div>
                        <div>2. MariaDB (<a class="link link-info" href="https://mariadb.org/download">Download</a> &amp; Install &amp; Add it to the Path)</div>
                        <div>3. bun</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"\\"}some{"\\"}folder{"\\"}</i></code></pre>
                            <pre data-prefix="$"><code>powershell -c "irm bun.sh/install.ps1 | iex"</code></pre>
                            <pre data-prefix="$"><code>bun -v</code></pre>
                        </div>
                        <div class="divider" />
                        <h3 class="text-xl text-center font-bold">
                            Setup:
                        </h3>
                        <div>1. Download the project</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"\\"}some{"\\"}folder{"\\"}</i></code></pre>
                            <pre data-prefix="$"><code>git clone https://gitlab.com/estoesunademo/hideorhunt.git</code></pre>
                            <pre data-prefix="$"><code>cd .{"\\"}hideorhunt{"\\"}db{"\\"}</code></pre>
                        </div>
                        <div>2. Create the database</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"\\"}hideorhunt{"\\"}db{"\\"}</i></code></pre>
                            <pre data-prefix="$"><code>mysql -u {"{"}username{"}"} -p</code></pre>
                            <pre><code>MariaDB [(none)]{">"} source .{"\\"}db.sql</code></pre>
                            <pre><code>MariaDB [hoh]{">"} exit</code></pre>
                        </div>
                        <div>3. Edit the env file</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"\\"}hideorhunt{"\\"}db{"\\"}</i></code></pre>
                            <pre data-prefix="$"><code>copy .example.env .env</code></pre>
                            <pre data-prefix="$"><code>notepad .env</code></pre>
                            <pre><code></code></pre>
                            <pre><code class="text-neutral-500"><i># Edit the file with your values</i></code></pre>
                            <pre data-prefix="1"><code>DB_HOST="localhost"</code></pre>
                            <pre data-prefix="2"><code>DB_PORT=3306</code></pre>
                            <pre data-prefix="3"><code>DB_NAME="hoh"</code></pre>
                            <pre data-prefix="4"><code>DB_USER="username"</code></pre>
                            <pre data-prefix="5"><code>DB_PASS="password"</code></pre>
                            <pre data-prefix="6"><code>GAME_PASSWORD="pastanaga"</code></pre>
                        </div>
                        <div>4. Install dependencies &amp; run</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"\\"}hideorhunt{"\\"}db{"\\"}</i></code></pre>
                            <pre data-prefix="$"><code>bun install</code></pre>
                            <pre data-prefix="$"><code>bun run dev</code></pre>
                            <pre><code></code></pre>
                            <pre><code class="text-neutral-500"><i># If you see this everything should be good!</i></code></pre>
                            <pre data-prefix="$"><code>🦊 Elysia is running at localhost:3000</code></pre>
                        </div>
                    </div>
                </div>

                <input type="radio" name="tab-group" role="tab" class="tab" aria-label="Linux" />
                <div role="tabpanel" class="tab-content bg-base-100 border-base-300 rounded-box p-8">
                    <h2 class="text-2xl text-center">
                        🐧 Linux
                    </h2>
                    <div class="divider" />
                    <div class="flex flex-col gap-4">
                        <h3 class="text-xl text-center font-bold">
                            Prerequisites:
                        </h3>
                        <div>1. git</div>
                        <div role="tablist" class="tabs tabs-lifted">
                            <input type="radio" name="git-linux-tabs" role="tab" class="tab [--tab-bg:#2a323c]" aria-label="Debian" checked />
                            <div role="tabpanel" class="tab-content border-base-300 rounded-box bg-[#2a323c]">
                                <div class="mockup-code">
                                    <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                                    <pre data-prefix="$"><code>sudo apt install git</code></pre>
                                </div>
                            </div>

                            <input type="radio" name="git-linux-tabs" role="tab" class="tab [--tab-bg:#2a323c]" aria-label="Fedora" />
                            <div role="tabpanel" class="tab-content border-base-300 rounded-box bg-[#2a323c]">
                                <div class="mockup-code">
                                    <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                                    <pre data-prefix="$"><code>sudo dnf install git</code></pre>
                                </div>
                            </div>

                            <input type="radio" name="git-linux-tabs" role="tab" class="tab [--tab-bg:#2a323c]" aria-label="Arch Linux" />
                            <div role="tabpanel" class="tab-content border-base-300 rounded-box bg-[#2a323c]">
                                <div class="mockup-code">
                                    <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                                    <pre data-prefix="$"><code>sudo pacman -S git</code></pre>
                                </div>
                            </div>

                            <input type="radio" name="git-linux-tabs" role="tab" class="tab [--tab-bg:#2a323c]" aria-label="OpenSUSE" />
                            <div role="tabpanel" class="tab-content border-base-300 rounded-box bg-[#2a323c]">
                                <div class="mockup-code">
                                    <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                                    <pre data-prefix="$"><code>zypper install git</code></pre>
                                </div>
                            </div>
                        </div>
                        <div>2. MariaDB</div>
                        <div role="tablist" class="tabs tabs-lifted">
                            <input type="radio" name="mariadb-linux-tabs" role="tab" class="tab [--tab-bg:#2a323c]" aria-label="Debian" checked />
                            <div role="tabpanel" class="tab-content border-base-300 rounded-box bg-[#2a323c]">
                                <div class="mockup-code">
                                    <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                                    <pre data-prefix="$"><code>sudo apt install mariadb-server</code></pre>
                                    <pre data-prefix="$"><code>sudo mysql_secure_installation</code></pre>
                                    <pre data-prefix="$"><code>sudo systemctl enable --now mariadb</code></pre>
                                </div>
                            </div>

                            <input type="radio" name="mariadb-linux-tabs" role="tab" class="tab [--tab-bg:#2a323c]" aria-label="Fedora" />
                            <div role="tabpanel" class="tab-content border-base-300 rounded-box bg-[#2a323c]">
                                <div class="mockup-code">
                                    <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                                    <pre data-prefix="$"><code>sudo dnf install mariadb-server</code></pre>
                                    <pre data-prefix="$"><code>sudo mysql_secure_installation</code></pre>
                                    <pre data-prefix="$"><code>sudo systemctl enable --now mariadb</code></pre>
                                </div>
                            </div>

                            <input type="radio" name="mariadb-linux-tabs" role="tab" class="tab [--tab-bg:#2a323c]" aria-label="Arch Linux" />
                            <div role="tabpanel" class="tab-content border-base-300 rounded-box bg-[#2a323c]">
                                <div class="mockup-code">
                                    <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                                    <pre data-prefix="$"><code>sudo pacman -S mariadb</code></pre>
                                    <pre data-prefix="$"><code>sudo mysql_secure_installation</code></pre>
                                    <pre data-prefix="$"><code>sudo systemctl enable --now mariadb</code></pre>
                                </div>
                            </div>

                            <input type="radio" name="mariadb-linux-tabs" role="tab" class="tab [--tab-bg:#2a323c]" aria-label="OpenSUSE" />
                            <div role="tabpanel" class="tab-content border-base-300 rounded-box bg-[#2a323c]">
                                <div class="mockup-code">
                                    <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                                    <pre data-prefix="$"><code>zypper install mariadb-server</code></pre>
                                    <pre data-prefix="$"><code>sudo mysql_secure_installation</code></pre>
                                    <pre data-prefix="$"><code>sudo systemctl enable --now mariadb</code></pre>
                                </div>
                            </div>
                        </div>
                        <div>3. bun</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                            <pre data-prefix="$"><code>curl -fsSL https://bun.sh/install | bash</code></pre>
                            <pre data-prefix="$"><code>bun -v</code></pre>
                        </div>
                        <div class="divider" />
                        <h3 class="text-xl text-center font-bold">
                            Setup:
                        </h3>
                        <div>1. Download the project</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"/"}some{"/"}directory{"/"}</i></code></pre>
                            <pre data-prefix="$"><code>git clone https://gitlab.com/estoesunademo/hideorhunt.git</code></pre>
                            <pre data-prefix="$"><code>cd ./hideorhunt/db/</code></pre>
                        </div>
                        <div>2. Create the database</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"\\"}hideorhunt{"\\"}db{"\\"}</i></code></pre>
                            <pre data-prefix="$"><code>mariadb -u {"{"}username{"}"} -p</code></pre>
                            <pre><code>MariaDB [(none)]{">"} source ./db.sql</code></pre>
                            <pre><code>MariaDB [hoh]{">"} exit</code></pre>
                        </div>
                        <div>3. Edit the env file</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"/"}hideorhunt{"/"}db{"/"}</i></code></pre>
                            <pre data-prefix="$"><code>cp .example.env .env</code></pre>
                            <pre data-prefix="$"><code>vim .env</code></pre>
                            <pre><code></code></pre>
                            <pre><code class="text-neutral-500"><i># Edit the file with your values</i></code></pre>
                            <pre data-prefix="1"><code>DB_HOST="localhost"</code></pre>
                            <pre data-prefix="2"><code>DB_PORT=3306</code></pre>
                            <pre data-prefix="3"><code>DB_NAME="hoh"</code></pre>
                            <pre data-prefix="4"><code>DB_USER="username"</code></pre>
                            <pre data-prefix="5"><code>DB_PASS="password"</code></pre>
                            <pre data-prefix="6"><code>GAME_PASSWORD="pastanaga"</code></pre>
                        </div>
                        <div>4. Install dependencies &amp; run</div>
                        <div class="mockup-code">
                            <pre><code class="text-neutral-500"><i>..{"\\"}hideorhunt{"\\"}db{"\\"}</i></code></pre>
                            <pre data-prefix="$"><code>bun install</code></pre>
                            <pre data-prefix="$"><code>bun run dev</code></pre>
                            <pre><code></code></pre>
                            <pre><code class="text-neutral-500"><i># If you see this everything should be good!</i></code></pre>
                            <pre data-prefix="$"><code>🦊 Elysia is running at localhost:3000</code></pre>
                        </div>
                    </div>
                </div>

                <input type="radio" name="tab-group" role="tab" class="tab" aria-label="MacOS" />
                <div role="tabpanel" class="tab-content bg-base-100 border-base-300 rounded-box p-8">
                    <h2 class="text-2xl text-center">
                        🍎 MacOS
                    </h2>
                    <div class="divider" />
                    <div class="flex flex-col gap-4">
                        <div class="mockup-code">
                            <pre data-prefix="$"><code>idk lol</code></pre>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Hosting;
