function Navbar() {
    return (
        <nav class="z-50 navbar bg-base-100 sticky top-0 flex flex-row gap-4">
            <div class="flex-1">
                <a class="btn btn-ghost text-xl" href="/">Hide Or Hunt</a>
            </div>
            <div class="flex-none">
                <ul class="menu menu-horizontal px-1">
                    <li>
                        <a class="btn btn-ghost link link-info" href="/hosting">
                            <svg class="size-4 fill-info" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path d="M48 80a48 48 0 1 1 96 0A48 48 0 1 1 48 80zM0 224c0-17.7 14.3-32 32-32H96c17.7 0 32 14.3 32 32V448h32c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H64V256H32c-17.7 0-32-14.3-32-32z" /></svg>
                            How to host
                        </a>
                    </li>
                    <li>
                        <a class="btn btn-primary" href="/register">
                            <svg class="size-4 fill-primary-content" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M96 128a128 128 0 1 1 256 0A128 128 0 1 1 96 128zM0 482.3C0 383.8 79.8 304 178.3 304h91.4C368.2 304 448 383.8 448 482.3c0 16.4-13.3 29.7-29.7 29.7H29.7C13.3 512 0 498.7 0 482.3zM504 312V248H440c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V136c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H552v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" /></svg>
                            Register
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Navbar;
