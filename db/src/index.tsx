import { Elysia, t } from "elysia";
import { html } from '@elysiajs/html'
import { staticPlugin } from '@elysiajs/static'
import mysql from "mysql2/promise";
import BaseHtml from "./BaseHtml";
import Test from "./Test";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Alert from "./Alert";
import { registerUser } from "./models/register";
import { loginUser } from "./login";
import { submitScores } from "./models/submitScores";
import Hosting from "./pages/Hosting";

export const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    port: Number(process.env.DB_PORT),
    ///
    waitForConnections: true,
    connectionLimit: 10,
    maxIdle: 10,
    idleTimeout: 60000,
    queueLimit: 0,
    enableKeepAlive: true,
    keepAliveInitialDelay: 0,
});

const app = new Elysia()
    .use(staticPlugin())
    .use(html())
    .get("/test", () => <Test />)
    .get("/", () =>
        <BaseHtml>
            <Home />
        </BaseHtml>)
    .get("/register", () =>
        <BaseHtml>
            <Register />
        </BaseHtml>)
    .get("/hosting", () =>
        <BaseHtml>
            <Hosting />
        </BaseHtml>)
    .post("/register", async ({ body }) => {
        let [msg, status] = await registerUser(body.username, body.password);
        return <Alert msg={msg} status={status} />
    }, {
        body: t.Object({
            username: t.String(),
            password: t.String()
        })
    })
    .post("/login", async ({ body }) => {
        let [msg, status, isValid] = await loginUser(body.username, body.password);
        return { msg, status, isValid };
    }, {
        body: t.Object({
            username: t.String(),
            password: t.String()
        })
    })
    .post("/validate", ({ body }) => {
        console.log("/validate");
        return { isValid: body.password === process.env.GAME_PASSWORD }
    }, {
        body: t.Object({
            password: t.String()
        })
    })
    .post("/submit", async ({ body }) => {
        console.log("/submit");
        const res = await submitScores(body.password, body.data);
        return { success: res }
    }, {
        body: t.Object({
            password: t.String(),
            data: t.Array(t.Object({
                username: t.String(),
                score: t.Number()
            }))
        })
    })
    .listen(3000);

console.log(`🦊 Elysia is running at ${app.server?.hostname}:${app.server?.port}`);
